import pandas as pd
from sklearn.tree import DecisionTreeClassifier
import pickle

# Load data from internet source. See census-data-info.txt for column information.
data = pd.read_csv("https://archive.ics.uci.edu/ml/machine-learning-databases/census-income/census-income.data", sep=",", header = None)
X = data.loc[:, [0, 4, 12]]
Y = data.loc[:, 14]==" >50K"

# Fit a decision tree
# https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html
decision_tree_model = DecisionTreeClassifier()
decision_tree_model.fit(X, Y)

# Serialize to disk
with open("census_model.pkl", "wb") as f:
	pickle.dump(decision_tree_model, f)